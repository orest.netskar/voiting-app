provider "aws" {
  region = "us-east-2"
}

data "aws_availability_zones" "all" {}

# resource "aws_instance" "myVoitingApp" {
#     count = 2
#     ami = "ami-0fc20dd1da406780b"
#     instance_type = "t2.micro"
#     tags = {
#         Name = " build by terraform myVoitingApp ${count.index}"
#         Owner = "Orest Netskar"
#     }
#     vpc_security_group_ids = [aws_security_group.my_webserver.id]
#     user_data = file("user_data.sh")

# }

resource "aws_security_group" "my_webserver" {
  name        = "WebServer Security Group"
  description = "WebServer Security Group"
 

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_launch_configuration" "voidapp" {
  name = "terraform-launch-config-voidapp"
  image_id = "ami-0fc20dd1da406780b"
  instance_type          = "t2.micro"
  security_groups        = [aws_security_group.my_webserver.id]
  # key_name               = "${var.key_name}"
  user_data = file("user_data.sh")
  lifecycle {
    create_before_destroy = true
  }
  # tag {
  #   key = "Name"
  #   value = "terraform-launch-config-voidapp"
  # }
}
resource "aws_autoscaling_group" "voidapp" {
  name = "autoscaling_group_voidapp"
  launch_configuration = aws_launch_configuration.voidapp.id
  desired_capacity = 2
  availability_zones = ["us-east-2c", "us-east-2a", "us-east-2b"]
  # availability_zones = ["${data.aws_availability_zones.all.names}"]
  min_size = 1
  max_size = 5
  load_balancers = ["${aws_elb.voidapp.name}"]
  health_check_type = "ELB"
  tag {
    key = "Name"
    value = "terraform-asg-voidapp"
    propagate_at_launch = true
  }
}
## Security Group for ELB
resource "aws_security_group" "elb" {
  name = "terraform-voidapp-elb"
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
### Creating ELB
resource "aws_elb" "voidapp" {
  name = "terraform-elb-voidapp"
  security_groups = ["${aws_security_group.elb.id}"]
  availability_zones = ["us-east-2c", "us-east-2a", "us-east-2b"]
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    interval = 30
    target = "HTTP:80/"
  }
  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = "80"
    instance_protocol = "http"
  }
}